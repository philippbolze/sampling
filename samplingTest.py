import unittest
from datetime import datetime

from sampling import get_sample_time, sample
from sampling_controller import read_input, result_to_sring


class TestSample(unittest.TestCase):


    def test_get_sample_time(self):
        # GIVEN
        input_time = datetime.strptime("2017-01-03T10:04:45", "%Y-%m-%dT%H:%M:%S")
        expected_sample_time = datetime.strptime("2017-01-03T10:05:00", "%Y-%m-%dT%H:%M:%S")

        # WHEN
        sample_time = get_sample_time(input_time)

        # THEN
        self.assertEqual(sample_time, expected_sample_time)

    def test_get_sample_time_on_grid(self):
        # GIVEN
        input_time = datetime.strptime("2017-01-03T10:30:00", "%Y-%m-%dT%H:%M:%S")

        # WHEN
        sample_time = get_sample_time(input_time)

        # THEN
        self.assertEqual(sample_time, input_time)

    def test_get_sample_time_overflow(self):
        # GIVEN
        input_time = datetime.strptime("2017-01-03T23:57:42", "%Y-%m-%dT%H:%M:%S")
        expected_sample_time = datetime.strptime("2017-01-04T00:00:00", "%Y-%m-%dT%H:%M:%S")

        # WHEN
        sample_time = get_sample_time(input_time)

        # THEN
        self.assertEqual(sample_time, expected_sample_time)

    def test_sample(self):
        # GIVEN
        measurements = read_input('test_input.json')

        # WHEN
        samples = sample(measurements)

        # THEN
        result_str = result_to_sring(samples)
        expected_str = open("expected_result.out", "r").read()
        self.assertEqual(expected_str, result_str)


if __name__ == '__main__':
    unittest.main()
