from datetime import datetime, timedelta
from Measurement import Measurement


global DATE_FORMAT
DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"


def sample(unsampled_measurements):
    measurements_by_type = {}
    for measurement in unsampled_measurements:
        type_dictionary = measurements_by_type.get(measurement.type)
        if type_dictionary is None:
            type_dictionary = {}
            measurements_by_type[measurement.type] = type_dictionary
        update_if_necessary(type_dictionary, measurement)
    return build_result(measurements_by_type)


def update_if_necessary(type_dictionary, measurement):
    measurement_time = datetime.strptime(measurement.time, DATE_FORMAT)
    sample_time = get_sample_time(measurement_time)
    current_best_measurement = type_dictionary.get(sample_time)
    if current_best_measurement is None:
        type_dictionary[sample_time] = measurement
    else:
        current_best_time = datetime.strptime(current_best_measurement.time, DATE_FORMAT)
        if measurement_time > current_best_time:
            type_dictionary[sample_time] = measurement


def get_sample_time(measurement_time):
    minute_modulo = measurement_time.minute % 5
    minutes_to_add = - minute_modulo
    if not (minute_modulo == 0 and measurement_time.second == 0):
        minutes_to_add += 5
    sample_time = measurement_time.replace(second=0)
    sample_time = sample_time + timedelta(minutes=minutes_to_add)
    return sample_time


def build_result(types_dictionary):
    samples = {}
    for type_dictionary in types_dictionary.values():
        for sample_time in sorted(type_dictionary.keys()):
            original_measurement = type_dictionary[sample_time]
            original_type = original_measurement.type
            type_list = samples.get(original_type)
            if type_list is None:
                type_list = []
                samples[original_type] = type_list
            sample_time_str = sample_time.strftime(DATE_FORMAT)
            sample_measurement = Measurement(sample_time_str, original_measurement.value, original_measurement.type)
            type_list.append(sample_measurement)
    return samples
