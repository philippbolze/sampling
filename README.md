**Sampling**

The sampling algorithm can be found in file **sampling.py**


**Features**

• Every measurement type will be sampled seperately

• Given an interval of five minutes, only the last one will be selected 

• If the value is exactly on the grid, it is considered part of the current interval

• The input values are not sorted in any way

• The result must be sorted (earliest first)


written for Python 2.7