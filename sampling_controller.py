import json
from sampling import sample
from Measurement import Measurement


def read_input(input_file):
    measurements = []
    with open(input_file) as json_file:
        data = json.load(json_file)
        for element in data:
            measurements.append(Measurement(element['time'], element['value'], element['type']))
    return measurements


def result_to_sring(result):
    string = ""
    for type_list in result.values():
        string += measurements_to_string(type_list)
    return string


def measurements_to_string(measurements):
    string = ""
    for sample in measurements:
        string += str(sample) + "\n"
    return string


input_file = 'test_input.json'
unsampled_measurements = read_input(input_file)
result = sample(unsampled_measurements)
print "INPUT:"
print measurements_to_string(unsampled_measurements)
print "OUTPUT:"
print result_to_sring(result)
