class Measurement:
    def __init__(self, time, value, type):
        self.time = time
        self.value = value
        self.type = type

    def __str__(self):
        return "{" + self.time + ", " + self.type + ", " + str(self.value) + "}"
